﻿Shader "CgClass/RGBShader" { // defines the name of the shader 
	SubShader { // Unity chooses the subshader that fits the GPU best
		Pass { // some shaders require multiple passes
			CGPROGRAM 

			#pragma vertex vert // vert function is the vertex shader 
			#pragma fragment frag // frag function is the fragment shader

			// for multiple vertex output parameters an output structure 
			// is defined.
			// This structure is ths output of vertex operation and is input of fragment operation.
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 col : TEXCOORD0;
			};

			//
			vertexOutput vert(float4 vertexPos : POSITION) 
			// vertex shader 
			{
				vertexOutput output; // we don't need to type 'struct' here

				output.pos =  mul(UNITY_MATRIX_MVP, vertexPos);
				output.col = vertexPos + float4(0.5, 0.5, 0.5, 0.0);
				// Here the vertex shader writes output data
				// to the output structure. We add 0.5 to the 
				// x, y, and z coordinates, because the 
				// coordinates of the cube are between -0.5 and
				// 0.5 but we need them between 0.0 and 1.0. 
				return output;
			}

			float4 frag(vertexOutput input) : COLOR // fragment shader
			{
				return input.col; 
				// Here the fragment shader returns the "col" input 
				// parameter with semantic TEXCOORD0 as nameless
				// output parameter with semantic COLOR.
			}

			ENDCG 

			// this commented code do the same thing, but doesn't use structure.
			//CGPROGRAM 

			//#pragma vertex vert // vert function is the vertex shader 
			//#pragma fragment frag // frag function is the fragment shader

			//void vert(float4 vertexPos : POSITION,
			//   out float4 pos : SV_POSITION,
			//   out float4 col : TEXCOORD0)  
			//{
			//   pos =  mul(UNITY_MATRIX_MVP, vertexPos);
			//   col = vertexPos + float4(0.5, 0.5, 0.5, 0.0);
			//   return;
			//}

			//float4 frag(float4 pos : SV_POSITION, 
			//   float4 col : TEXCOORD0) : COLOR 
			//{
			//   return col; 
			//}

			//ENDCG
		}
	}
}